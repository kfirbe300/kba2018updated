<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\Task;
use app\models\Projectpartner;
use app\models\Project;
use yii\web\UnauthorizedHttpException;
use yii\helpers\ArrayHelper;

class Taskpartner extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'taskPartner';
    }

    public function rules()
    {
        return [
            [['userId', 'taskId'], 'required'],
            [['userId', 'taskId'], 'integer'],
			[['userId', 'taskId'], 'unique', 'targetAttribute' => ['userId', 'taskId'] , 'message' => 'Already Exists!']
        ];
    }

    public function attributeLabels()
    {
        return [
            'userId' => 'User',
            'taskId' => 'Task',
        ];
    }
	
	public function getUserItem()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
	
	public function getTaskItem()
    {
        return $this->hasOne(Task::className(), ['id' => 'taskId']);
    }
	
	public function getProjectpartnerItem()
    {
        return $this->hasOne(Projectpartner::className(), ['userId' => 'userId']);
    }
	
	public function getProjectItem()
    {
        return $this->hasOne(Project::className(), ['responsible' => 'userId']);
    }
	
	//שיוך משתמש שלא קשור למשימה
	public function beforeSave($insert) 
    {		
		$return = parent::beforeSave($insert);
		$flag = false;
		$selection = Projectpartner::find()->all();
		//foreach(self::getProjectpartnerItem() as $i){
		foreach($selection as $i){
			//if(($this->getAttribute('taskId') == $i->taskItem->id) && ($i->taskItem->project == $i->projectpartnerItem->projectId) && ($i->projectpartnerItem->userId == $this->getAttribute('userId')))
			//בדיקה האם המשתמש שותף לפרויקט
			if(($i->userId == $this->getAttribute('userId'))){
				if(($this->taskItem->project == $i->projectId)){
					$flag = true;
				}
			}
			
		}
		
		//בדיקה האם המשתמש אחראי לפרויקט
		$checkTask = Task::findOne($this->getAttribute('taskId'));
		if($this->getAttribute('userId') == $checkTask->projectItem->responsible){
			//if($this->getAttribute('taskId') == $this->taskItem->id){
				//if($this->projectItem->id == $this->taskItem->project)
					$flag = true;
			//}
		}
		
		if(!$flag){
			throw new UnauthorizedHttpException ('Hey, this user does not belong to the project.');
		}
		
		//משתמש חייב להיות מוגדר כמבצע משימה
		$role = Yii::$app->authManager->getRolesByUser($this->userItem->id);
		if($this->getAttribute('userId')){
			if(implode(', ', array_keys($role)) != 'Perform Task')
				throw new UnauthorizedHttpException ('Hey, this user can not be task partner, only a perform task.');
		}
		
		return $return;	
    }
	
	public function getFullname()
    {
        return $this->userItem->fullname.' belong to '.$this->taskItem->name;
    }
	
	//יוזרים פעילים
	public static function getExistUsersWithAllUsers(){
		$user = new Taskpartner();
		
		$userExist = $user->find()->select('userId')->distinct()->all();
		$userExistArr = [];
		
		foreach($userExist as $i){
			$userExistArr[] = $i->userId;
		}
		//נמצא אחראים קיימים
		$alluser = User::find()->where(['id' => $userExistArr])->all();
		$alluserArray = ArrayHelper::
					map($alluser, 'id', 'fullname');
		
		$alluserArray[null] = 'All';
		$alluserArray = array_reverse ($alluserArray, true );
		return $alluserArray;
	}
	
	public static function getExistTasksWithAllTasks(){
		$task = new Taskpartner();
		$id = Yii::$app->user->identity->id;
		$taskExist = $task->find()->select('taskId')->distinct()->all();
		$taskExistArr = [];
		
		if(!\Yii::$app->user->can('createProject')){
			$partners = Taskpartner::find()->all();
			
			if(\Yii::$app->user->can('createTask')){
				$tasks = Task::find()->all();
				foreach($tasks as $i){
					foreach($partners as $j){
						if($i->projectItem->responsible == $id){
							if($i->id == $j->taskId){
								$taskExistArr[] = $j->taskId;
							}
						}
						else{
							$allProjectPartners = Projectpartner::find()->all();
							foreach($allProjectPartners as $p){
								if($p->userId == $id){
									if($i->projectItem->id == $p->projectId){
										$taskExistArr[] = $i->id;
									}
								}
							}
						}
					}
				}
			}
			else if(\Yii::$app->user->can('Perform Task')){
				foreach($partners as $j){
					if($j->userId == $id)
						$taskExistArr[] = $j->taskId;
				}
			}
			else{
				foreach($taskExist as $i){
					$taskExistArr[] = $i->taskId;
				}
			}
		}
		else{
			foreach($taskExist as $i){
				$taskExistArr[] = $i->taskId;
			}
		}
		
		
		//נמצא אחראים קיימים
		$alltask = Task::find()->where(['id' => $taskExistArr])->all();
		$alltaskArray = ArrayHelper::
					map($alltask, 'id', 'name');
		
		$alltaskArray[null] = 'All';
		$alltaskArray = array_reverse ($alltaskArray, true );
		return $alltaskArray;
	}
}
<?php

namespace app\models;

use Yii;
use yii\widgets\ActiveForm;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\models\Status;
use app\models\Subcontractor;
use app\models\User;
use app\models\Type;
use app\models\Task;
use app\models\Projectpartner;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\UnauthorizedHttpException;
use dosamigos\taggable\Taggable;

class Project extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'project';
    }

    public function rules()
    {
        return [
			[['name', 'type', 'responsible', 'cost', 'status', 'startDate', 'finishDate'], 'required'],
			[['tagNames'],'safe'],
            [['type', 'responsible', 'department', 'cost', 'subcontractor', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['startDate', 'finishDate', 'actualfinishDate'], 'safe'],
            [['name'], 'string', 'max' => 255],
			[['name'], 'match', 'pattern' => '/^[a-zA-Z0-9- ]+$/', 'message' => 'Error! only letters'],
			[['name', 'department'], 'unique', 'targetAttribute' => ['name', 'department'] , 'message' => 'Already Exists!']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'responsible' => 'Responsible',
            'department' => 'Department',
            'cost' => 'Cost ($)',
            'subcontractor' => 'Subcontractor',
            'status' => 'Status',
            'startDate' => 'Start Date',
            'finishDate' => 'Finish Date',
            'actualfinishDate' => 'Actual Finish Date',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
	
	public static function getProjects()
	{
		$allProjects = self::find()->all();
		
		$allProjectsArray;
		foreach($allProjects as $project){
			if(($project->status != '1') && ($project->status != '2'))
				$allProjectsArray[$project->id] = $project->name;			
		}
		/*$allProjectsArray = ArrayHelper::
					map($allProjects, 'id', 'name');*/
		return $allProjectsArray;						
	}
	
	public static function getProjectofuser()
	{
		$allProjectofuser = self::find()->all();
		$res = Yii::$app->user->identity->id;
		$allProjectofuserArray;
		foreach($allProjectofuser as $project){
			if($project->responsible == $res){
				if(($project->status != '1') && ($project->status != '2'))
					$allProjectofuserArray[$project->id] = $project->name;
			}
						
		}
		return $allProjectofuserArray;						
	}
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	
	public function getDepartmentItem()
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }
	
	public function getSubcontractorItem()
    {
        return $this->hasOne(Subcontractor::className(), ['id' => 'subcontractor']);
    }
	
	public function getResponsibleItem()
    {
        return $this->hasOne(User::className(), ['id' => 'responsible']);
    }
	
	public function getTypeItem()
    {
        return $this->hasOne(Type::className(), ['id' => 'type']);
    }
	
	//הצגת כל המשתמשים הקשורים לפרויקט
	public function getUsersItem(){
		return $this->hasMany(Projectpartner::className(), ['projectId' => 'id']);
		
	}
	
	public function getTaskItem(){
		return $this->hasOne(Task::className(), ['project' => 'id']);
	}
	
	//הצגת כל המשימות הקשורות לפרויקט
	public function getTasksItem(){
		return $this->hasMany(Task::className(), ['project' => 'id']);
		
	}
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
	}
	
	public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('project_tag_assn', ['project_id' => 'id']);
    }
	
	//הגדרת תאריך ויוזר אוטמטית לאחר יצירה או עדכון
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
			Taggable::className(), 
		];
    }
	
	public static function getProjectsWithAllStatuses()
	{
		$allProjects = self::getProjects();
		$allProjects[null] = 'All';
		$allProjects = array_reverse ( $allProjects, true );
		return $allProjects;	
	}
	
	public function beforeSave($insert){	
		//הוספת מניעת האפשרות שמנהל פרויקט יוכל לשנות אחראי	
		$return = parent::beforeSave($insert);
		
		//ולידציה של התאריכים
		if($this->getAttribute('startDate') > $this->getAttribute('finishDate')){
			throw new UnauthorizedHttpException ('Hey, start date can not be after finish date.');
		}
		
		//אין אפשרות להגדיר תאריך התחלה לפני התאריך של היום
		if ($this->isNewRecord){
			if($this->getAttribute('startDate') < date('Y-m-d')){
				throw new UnauthorizedHttpException ('Hey, you can not set a start date before todays date.');
			}
		}
		
		//כאשר הפרויקט מבוצע יוגדר אוטומטית תאריך סיום
		if($this->getAttribute('status') == 1){
			$this->actualfinishDate = date('Y-m-d');
		}
		else{
			$this->actualfinishDate = 0000-00-00;
		}
		
		//רק מי שמוגדר כמנהל פרויקט יכול להיות אחראי
		$role = Yii::$app->authManager->getRolesByUser($this->responsibleItem->id);
		if($this->getAttribute('responsible')){
			//if(!\Yii::$app->user->can('responsible'))
			if( implode(', ', array_keys($role)) != 'Project Manager')
				throw new UnauthorizedHttpException ('Hey, the user is not defined as a project manager');
		}
		
		//בדיקה שכל המשימות בוצעות
		$selection = Task::find()->all();
		if($this->isAttributeChanged('status')){
			if($this->getAttribute('status') == 1){
				foreach($selection as $id){
					if($this->getAttribute('id') == $id->project){
						if(($id->status != 2) && ($id->status != 1)){
							throw new UnauthorizedHttpException ('Hey, you can not close the project, there are open tasks.');
						}
					}
				}
			}
		}
		
		//התאמת המחלקה למשתמש האחראי
		$selectionUsers = User::find()->all();
		if(\Yii::$app->user->can('createProject')){
			foreach($selectionUsers as $id){
				if($this->getAttribute('responsible') == $id->id){
					$this->department = $id->department;
				}
			}
		}
		
		
		return $return;	
	}
	
	//מאפשר לסנן לפי אחראים פעילים
	public static function getExistResponsiblesWithAllResponsibles(){
		$res = new Project();
		
		$resExist = $res->find()->select('responsible')->distinct()->all();
		$resExistArr = [];
		
		foreach($resExist as $i){
			$resExistArr[] = $i->responsible;
		}
		
		$allResponsibles = User::find()->where(['id' => $resExistArr])->all();
		$allResponsiblesArray = ArrayHelper::
					map($allResponsibles, 'id', 'fullname');
		
		$allResponsiblesArray[null] = 'All';
		$allResponsiblesArray = array_reverse ($allResponsiblesArray, true );
		return $allResponsiblesArray;
	}
	
	//מחלקות פעילות
	public static function getExistDepartmentsWithAllDepartments(){
		$dep = new Project();
		
		$depExist = $dep->find()->select('department')->distinct()->all();
		$depExistArr = [];
		
		foreach($depExist as $i){
			$depExistArr[] = $i->department;
		}
		//נמצא אחראים קיימים
		$allDepartments = Department::find()->where(['id' => $depExistArr])->all();
		$allDepartmentsArray = ArrayHelper::
					map($allDepartments, 'id', 'name');
		
		$allDepartmentsArray[null] = 'All';
		$allDepartmentsArray = array_reverse ($allDepartmentsArray, true );
		return $allDepartmentsArray;
	}
	
	//סוגי פעילים
	public static function getExistTypesWithAllTypes(){
		$type = new Project();
		
		$typeExist = $type->find()->select('type')->distinct()->all();
		$typeExistArr = [];
		
		foreach($typeExist as $i){
			$typeExistArr[] = $i->type;
		}
		//נמצא אחראים קיימים
		$allTypes = Type::find()->where(['id' => $typeExistArr])->all();
		$allTypesArray = ArrayHelper::
					map($allTypes, 'id', 'type');
		
		$allTypesArray[null] = 'All';
		$allTypesArray = array_reverse ($allTypesArray, true );
		return $allTypesArray;
	}
	
	//סטטוסים פעילים
	public static function getExistStatusWithAllStatus(){
		$status = new Project();
		
		$statusExist = $status->find()->select('status')->distinct()->all();
		$statusExistArr = [];
		
		foreach($statusExist as $i){
			$statusExistArr[] = $i->status;
		}
		//נמצא אחראים קיימים
		$allstatus = Status::find()->where(['id' => $statusExistArr])->all();
		$allstatusArray = ArrayHelper::
					map($allstatus, 'id', 'name');
		
		$allstatusArray[null] = 'All';
		$allstatusArray = array_reverse ($allstatusArray, true );
		return $allstatusArray;
	}
}
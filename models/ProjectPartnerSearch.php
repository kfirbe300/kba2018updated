<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Projectpartner;
use app\models\Project;
use app\models\User;

class ProjectpartnerSearch extends ProjectPartner
{
    public function rules()
    {
        return [
            [['userId', 'projectId'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ProjectPartner::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		//כל השותפים שנמצאים עם המבצע תחת אותן פרויקטים
		if(!\Yii::$app->user->can('createTask')){
			if(\Yii::$app->user->can('Perform Task')){
				$perform = Yii::$app->user->identity->id;
				
				$allProjects = Projectpartner::find()->all();
				$allProjectsArray = [];
				
				foreach($allProjects as $j){
					if($j->userId == $perform){
						$allProjectsArray[] = $j->projectId;
					}
				}
				$query->andFilterWhere(['projectId' => $allProjectsArray]);
			}
		}
		
		//שותפים לפרויקט שמנהל פויקט שותף לו ואחראי עליו
		if(!\Yii::$app->user->can('createUser')){
			if (\Yii::$app->user->can('Project Manager')){
				$res = Yii::$app->user->identity->id;
					
				$projects = Project::find()->all();
							
				$allProjectPartners = Projectpartner::find()->all();
				$allProjectPartnersArray = [];
							
				foreach($projects as $i){
					foreach($allProjectPartners as $j){
						if($i->responsible == $res){
							if($i->id == $j->projectId){
								$allProjectPartnersArray[] = $i->id;
							}
						}
						else if($j->userId == $res){
							if($i->id == $j->projectId){
								$allProjectPartnersArray[] = $i->id;
							}
						}
					}
				}
				if($allProjectPartnersArray == null){
					$query->andFilterWhere(['projectId' => 0]);
				}
				else
					$query->andFilterWhere(['projectId' => $allProjectPartnersArray]);
			}
		}

        // grid filtering conditions
        $query->andFilterWhere([
            'userId' => $this->userId,
            'projectId' => $this->projectId,
        ]);

        return $dataProvider;
    }
}
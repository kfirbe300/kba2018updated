<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Subcontractor extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'subcontractor';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            //[['phone'], 'integer'],
            [['name', 'contact'], 'string', 'max' => 255],
			[['phone'], 'match', 'pattern' => '/^[0-9]{10}$/'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'contact' => 'Contact',
            'phone' => 'Phone',
        ];
    }
	
	public static function getSubcontractors()
	{
		$allSubcontractors = self::find()->all();
		//$allSubcontractors[null] = 'null';
		$allSubcontractorsArray = ArrayHelper::
					map($allSubcontractors, 'id', 'name');
		$allSubcontractorsArray[null] = 'Null';
		return $allSubcontractorsArray;						
	}
	
	public function getProjectsItem(){
		return $this->hasMany(Project::className(), ['subcontractor' => 'id']);
		
	}
}
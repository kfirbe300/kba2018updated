<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Taskpartner;
use app\models\Task;

class TaskpartnerSearch extends TaskPartner
{
    public function rules()
    {
        return [
            [['userId', 'taskId'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = TaskPartner::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		//כל השותפים שנמצאים עם המבצע תחת אותן משימות
		if(!\Yii::$app->user->can('createTask')){
			if(\Yii::$app->user->can('Perform Task')){
				$perform = Yii::$app->user->identity->id;
				
				$allProjects = Taskpartner::find()->all();
				$allProjectsArray = [];
				
				foreach($allProjects as $j){
					if($j->userId == $perform){
						$allProjectsArray[] = $j->taskId;
					}
				}
				if($allProjectsArray == null){
					$query->andFilterWhere(['taskId' => 0]);
				}
				else
					$query->andFilterWhere(['taskId' => $allProjectsArray]);
			}
		}
		
		//מנהל פרויקט יראה את כל הפרטנרים שנמצאים במשימות תחת אחריותו
		if(!\Yii::$app->user->can('createUser')){
			if (\Yii::$app->user->can('Project Manager')){
				$res = Yii::$app->user->identity->id;
					
				$tasks = Task::find()->all();
							
				$allTaskPartners = Taskpartner::find()->all();
				$allTaskPartnersArray = [];
							
				foreach($tasks as $i){
					foreach($allTaskPartners as $j){
						if($i->projectItem->responsible == $res){
							if($i->id == $j->taskId){
								$allTaskPartnersArray[] = $j->taskId;
							}
						}
						else{
							$allProjectPartners = Projectpartner::find()->all();
							foreach($allProjectPartners as $p){
								if($p->userId == $res){
									if($i->projectItem->id == $p->projectId){
										$allTaskPartnersArray[] = $i->id;
									}
								}
							}
						}
					}
				}
				if($allTaskPartnersArray == null){
					$query->andFilterWhere(['taskId' => 0]);
				}
				else
					$query->andFilterWhere(['taskId' => $allTaskPartnersArray]);
			}
		}
		
        // grid filtering conditions
        $query->andFilterWhere([
            'userId' => $this->userId,
            'taskId' => $this->taskId,
        ]);

        return $dataProvider;
    }
}
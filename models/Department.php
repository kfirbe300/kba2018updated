<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Project;
use yii\web\UnauthorizedHttpException;

class Department extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'department';
    }
	
    public function rules()
    {
        return [
            [['name', 'headDepartment', 'budget'], 'required'],
            [['headDepartment', 'budget'], 'integer'],
            [['name'], 'string', 'max' => 255],
		    [['name'], 'match', 'pattern' => '/^[a-zA-Z ]+$/', 'message' => 'Error! only letters'],
			[['headDepartment'],'unique','message' => 'The user is already a head department!'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'headDepartment' => 'Head Of Department',
            'budget' => 'Budget',
        ];
    }
	
	public static function getDepartments()
	{
		$allDepartments = self::find()->all();
		$allDepartmentsArray = ArrayHelper::
					map($allDepartments, 'id', 'name');
		$allDepartmentsArray[null] = 'Null';
		return $allDepartmentsArray;						
	}
	
	public static function getDepartmentsWithAllStatuses()
	{
		$allDepartments = self::getDepartments();
		$allDepartments[null] = 'All';
		$allDepartments = array_reverse ( $allDepartments, true );
		return $allDepartments;	
	}
	
	public function getUserItem()
    {
        return $this->hasOne(User::className(), ['id' => 'headDepartment']);
    }
	
	public function beforeSave($insert){	
		$return = parent::beforeSave($insert);
		
		//מי שמוגדר כמבצע משימה לא יוכל להיות ראש מחלקה
		$role = Yii::$app->authManager->getRolesByUser($this->headDepartment);
		if($this->getAttribute('headDepartment')){
			if( (implode(', ', array_keys($role)) != 'Project Manager') && (implode(', ', array_keys($role)) != 'Admin'))
				throw new UnauthorizedHttpException ('Hey, the user is can not defined as a head of department');
		}
		
		return $return;	
	}
	
	//הצגת של המשתמשים הקשורים למחלקה
	public function getUsersItem(){
		return $this->hasMany(User::className(), ['department' => 'id']);
		
	}
	
	public function getProjectsItem(){
		return $this->hasMany(Project::className(), ['department' => 'id']);
		
	}
}
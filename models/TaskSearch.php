<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;
use app\models\Taskpartner;


class TaskSearch extends Task
{
    public function rules()
    {
        return [
            [['id', 'project', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name', 'description', 'startDate', 'finishDate', 'actualfinishDate'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Task::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'project' => $this->project,
            'status' => $this->status,
			//'responsible' => $this->projectItem->responsibleItem->id,
            'startDate' => $this->startDate,
            'finishDate' => $this->finishDate,
            'actualfinishDate' => $this->actualfinishDate,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);
		
		if(!\Yii::$app->user->can('createTask')){
			if(\Yii::$app->user->can('Perform Task')){
				$perform = Yii::$app->user->identity->id;
				
				$projectPartner = Task::find()->all();
				$allPartners = Taskpartner::find()->all();
				$allPartnersArray = [];
				
				foreach($projectPartner as $i){
					foreach($allPartners as $j){
						if($j->userId == $perform){
							if($i->id == $j->taskId){
								$allPartnersArray[] = $j->taskId;
							}
						}
					}
				}
				if($allPartnersArray == null){
					$query->andFilterWhere(['id' => 0]);
				}
				else
					$query->andFilterWhere(['id' => $allPartnersArray]);
			}
		}
		
		//משימות שהמנהל פרויקט אחראי עליהם
		if(!\Yii::$app->user->can('createUser')){
			if (\Yii::$app->user->can('Project Manager')){
				$res = Yii::$app->user->identity->id;
					
				$taskPartner = Task::find()->all();
				$partners = Projectpartner::find()->all();		
				$allTaskPartners = Project::find()->all();
				$allTaskPartnersArray = [];
							
				foreach($taskPartner as $i){
					foreach($allTaskPartners as $j){
						if($j->responsible == $res){
							if($i->project == $j->id){
								$allTaskPartnersArray[] = $i->project;
							}
						}
						
					}
				}
				
				if($allTaskPartnersArray == null){
					$query->andFilterWhere(['id' => 0]);
				}
				else
					$query->andFilterWhere(['project' => $allTaskPartnersArray]);
			}
		}

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);
			
		

        return $dataProvider;
    }
}
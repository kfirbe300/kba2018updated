<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $subject
 * @property string $message
 * @property string $email
 * @property int $phoneNumber
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject', 'message', 'email', 'phoneNumber'], 'required'],
            [['message', 'email'], 'string'],
            [['phoneNumber'], 'integer'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Subject: ✏️',
            'message' => 'Message: 📝',
            'email' => 'Your Email: 📧',
            'phoneNumber' => 'Your Phone Number: ☎️',
        ];
    }
}

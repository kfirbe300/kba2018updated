<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Project;
use app\models\Status;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\UnauthorizedHttpException;
use app\models\Taskpartner;

class Task extends \yii\db\ActiveRecord
{
	//public $responsible;
    public static function tableName()
    {
        return 'task';
    }

    public function rules()
    {
        return [
            [['name', 'project', 'status', 'startDate', 'finishDate'], 'required'],
            [['project', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['startDate', 'finishDate', 'actualfinishDate'], 'safe'],
            [['name'], 'string', 'max' => 255],
			[['name'], 'match', 'pattern' => '/^[a-zA-Z0-9- ]+$/', 'message' => 'Error! only letters'],
			[['name', 'project'], 'unique', 'targetAttribute' => ['name', 'project'] , 'message' => 'Already Exists!']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'project' => 'Project',
			//'responsible' => 'Responsible',
            'description' => 'Description',
            'status' => 'Status',
            'startDate' => 'Start Date',
            'finishDate' => 'Finish Date',
            'actualfinishDate' => 'Actual Finish Date',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
	
	public function getProjectItem()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
	
	//הגדרת תאריך ויוזר אוטמטית לאחר יצירה או עדכון
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	public static function getTasks()
	{
		$allTasks = self::find()->all();
		$allTasksArray;
		foreach($allTasks as $task){
			if(($task->status != '1') && ($task->status != '2'))
				$allTasksArray[$task->id] = $task->name;			
		}
		/*$allTasksArray = ArrayHelper::
					map($allTasks, 'id', 'name');*/
		return $allTasksArray;						
	}
	
	public static function getTasksofuser()
	{
		$allTasksofuser = self::find()->all();
		$res = Yii::$app->user->identity->id;
		$allTasksofuserArray;
		$allTaskspartner = Taskpartner::find()->all();
		
		foreach($allTasksofuser as $task){
			if($res == $task->projectItem->responsible){
				if(($task->status != '1') && ($task->status != '2'))
					$allTasksofuserArray[$task->id] = $task->name;
			}			
		}
		return $allTasksofuserArray;		
	}
	
	public static function getTasksWithAllStatuses()
	{
		$allTasks = self::getTasks();
		$allTasks[null] = 'All';
		$allTasks = array_reverse ( $allTasks, true );
		return $allTasks;	
	}
	
	//הצגת כל המשתמשים הקשורים למשימה
	public function getUsersItem(){
		return $this->hasMany(Taskpartner::className(), ['taskId' => 'id']);
		
	}
	
	public function beforeSave($insert){		
		$return = parent::beforeSave($insert);
		
		//ולידציה של התאריכים
		if($this->getAttribute('startDate') > $this->getAttribute('finishDate')){
			throw new UnauthorizedHttpException ('Hey, start date can not be after finish date.');
		}
		
		//אין אפשרות להגדיר תאריך התחלה לפני התאריך של היום רק ליצירה חדשה
		if ($this->isNewRecord){
			if($this->getAttribute('startDate') < date('Y-m-d')){
				throw new UnauthorizedHttpException ('Hey, you can not set a start date before todays date.');
			}
		}
		
		//מניעת האפשרות להגדיר תאריך לא תואם לפרויקט
		$checkStartPro = $this->projectItem->startDate;
		$checkFinishPro = $this->projectItem->finishDate;
		if($this->getAttribute('startDate') < $checkStartPro)
			throw new UnauthorizedHttpException ('Hey, start date does not match to project start date.');
		if($this->getAttribute('finishDate') > $checkFinishPro)
			throw new UnauthorizedHttpException ('Hey, finish date does not match to project finish date.');
		
		//כאשר הפרויקט מבוצע יוגדר אוטומטית תאריך סיום
		if($this->getAttribute('status') == 1){
			$this->actualfinishDate = date('Y-m-d');
		}
		else{
			$this->actualfinishDate = 0000-00-00;
		}
		//מניעת האפשרות לפתוח משימה לפרויקט שנגמר
		if(($this->projectItem->status == 1) || ($this->projectItem->status == 2)){
			throw new UnauthorizedHttpException ('Hey, project closed you can not add more tasks.');
		}
		
		if(!\Yii::$app->user->can('createTask') && !\Yii::$app->user->can('createUser')){
			$checkTasks = Taskpartner::find()->all();
			$user = Yii::$app->user->identity->id;
			$checkUserRes = $this->projectItem->responsible;
			$bool = false;
			
			//מניעת עדכון סטטוס על-ידי מי שלא מוגדר כמבצע משימה
			if($checkTasks != null){
				foreach($checkTasks as $i){
						if($user == $i->userId){
							if($this->getAttribute('id') == $i->taskId)
								$bool = true;
						}
				}
			}
		
			if(!$bool){
				unset($this->status);
			}
		}
		
		return $return;	
	}
	
	//סטטוסים פעילים
	public static function getExistStatusWithAllStatus(){
		$status = new Task();
		$id = Yii::$app->user->identity->id;
		$statusExist = $status->find()->select('status')->distinct()->all();
		$statusExistArr = [];
		
		foreach($statusExist as $i){
			$statusExistArr[] = $i->status;
		}
		//נמצא אחראים קיימים
		$allstatus = Status::find()->where(['id' => $statusExistArr])->all();
		$allstatusArray = ArrayHelper::
					map($allstatus, 'id', 'name');
		
		$allstatusArray[null] = 'All';
		$allstatusArray = array_reverse ($allstatusArray, true );
		return $allstatusArray;
	}
	
	//פרויקטים פעילים
	public static function getExistProjectsWithAllProjects(){
		$project = new Task();
		$id = Yii::$app->user->identity->id;
		
		$projectExist = $project->find()->select('project')->distinct()->all();
		$projectExistArr = [];
		if(!\Yii::$app->user->can('createProject')){
			$partners = Projectpartner::find()->all();
			
			if(\Yii::$app->user->can('createTask')){
				foreach($projectExist as $i){
					if($i->projectItem->responsible == $id)
						$projectExistArr[] = $i->project;
				
				}
			}
			else if(\Yii::$app->user->can('Perform Task')){
				foreach($partners as $j){
					if($j->userId == $id)
						$projectExistArr[] = $j->projectId;
				}
			}
			else{
				foreach($projectExist as $i){
					$projectExistArr[] = $i->project;
				}
			}
		}
		else{
			foreach($projectExist as $i){
				$projectExistArr[] = $i->project;
			}
		}
		
		$allproject = Project::find()->where(['id' => $projectExistArr])->all();
	
		//נמצא אחראים קיימים
		$allproject = Project::find()->where(['id' => $projectExistArr])->all();
		$allprojectArray = ArrayHelper::
					map($allproject, 'id', 'name');
		
		$allprojectArray[null] = 'All';
		$allprojectArray = array_reverse ($allprojectArray, true );
		return $allprojectArray;
	}
	
	

}
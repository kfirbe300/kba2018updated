<?php

namespace app\models;

use Yii;
use app\models\Project;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\web\UnauthorizedHttpException;

class Projectpartner extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'projectPartner';
    }

    public function rules()
    {
        return [
            [['userId', 'projectId'], 'required'],
            [['userId', 'projectId'], 'integer'],
			[['userId', 'projectId'], 'unique', 'targetAttribute' => ['userId', 'projectId'] , 'message' => 'Already Exists!']
        ];
    }

    public function attributeLabels()
    {
        return [
            'userId' => 'User',
            'projectId' => 'Project',
        ];
    }
	
	public function getProjectItem()
    {
        return $this->hasOne(Project::className(), ['id' => 'projectId']);
    }
	
	public function getUserItem()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
	
	public static function getProjectpartners()
	{
		$allDProjectpartners = self::find()->all();
		$allProjectpartnersArray = ArrayHelper::
					map($allProjectpartners, 'id', 'name');
		return $allProjectpartnersArray;						
	}
	
	public function beforeSave($insert){		
		$return = parent::beforeSave($insert);
		
		//אם המשתמש מוגדר כאחראי לא נגדיר אותו שוב כמבצע
		if($this->isAttributeChanged('userId')){
			if($this->getAttribute('userId') == $this->projectItem->responsible){
				throw new UnauthorizedHttpException ('Hey, the user is responsible for the project there is no need to be defined as a partner.');
			}
		}
		
		//אחראי פרויקט יכול להגדיר רק לפרויקט שלו
		$checkUser = Yii::$app->user->identity->id;
		if($this->isAttributeChanged('projectId')){
			if (!\Yii::$app->user->can('createProjectpartner')){
				if($this->getAttribute('projectId') == $this->projectItem->id){
					if($checkUser != $this->projectItem->responsible)
						throw new UnauthorizedHttpException ('Hey, you not responsible for this project.');
				}
			}
		}
		
		//אם לא מוגדר למשתמש תפקיד, לא ניתן להגדיר כמבצע פרויקט
		$role = Yii::$app->authManager->getRolesByUser($this->userItem->id);
		if($this->getAttribute('userId')){
			if((implode(', ', array_keys($role)) != 'Project Manager') && (implode(', ', array_keys($role)) != 'Perform Task') && (implode(', ', array_keys($role)) != 'Admin'))
				throw new UnauthorizedHttpException ('Hey, this user can not be project partner.');
		}
		
		
		
		return $return;	
	}
	
	public function getFullname()
    {
        return $this->userItem->fullname.' belong to '.$this->projectItem->name;
    }
	
	//יוזרים פעילים
	public static function getExistUsersWithAllUsers(){
		$user = new Projectpartner();
		$id = Yii::$app->user->identity->id;
		$userExist = $user->find()->select('userId')->distinct()->all();
		$userExistArr = [];
		
	
		foreach($userExist as $i){
			$userExistArr[] = $i->userId;
		}
		
		//נמצא אחראים קיימים
		$alluser = User::find()->where(['id' => $userExistArr])->all();
		$alluserArray = ArrayHelper::
					map($alluser, 'id', 'fullname');
		
		$alluserArray[null] = 'All';
		$alluserArray = array_reverse ($alluserArray, true );
		return $alluserArray;
	}
	
	//פרויקטים פעילים
	public static function getExistProjectsWithAllProjects(){
		$project = new Projectpartner();
		$id = Yii::$app->user->identity->id;
		$projectExist = $project->find()->select('projectId')->distinct()->all();
		$projectExistArr = [];
		
		if(!\Yii::$app->user->can('createProject')){
			$partners = Projectpartner::find()->all();
			
			if(\Yii::$app->user->can('createTask')){
				foreach($projectExist as $i){
					if($i->projectItem->responsible == $id)
						$projectExistArr[] = $i->projectId;
					else{
						foreach($partners as $j){
							if($j->userId == $id)
								$projectExistArr[] = $j->projectId;
						}		
					}
				}
			}
			else if(\Yii::$app->user->can('Perform Task')){
				foreach($partners as $j){
					if($j->userId == $id)
						$projectExistArr[] = $j->projectId;
				}
			}
			else{
				foreach($projectExist as $i){
					$projectExistArr[] = $i->projectId;
				}
			}
		}
		else{
			foreach($projectExist as $i){
				$projectExistArr[] = $i->projectId;
			}
		}
		/*foreach($projectExist as $i){
			$projectExistArr[] = $i->projectId;
		}*/
		//נמצא אחראים קיימים
		$allproject = Project::find()->where(['id' => $projectExistArr])->all();
		$allprojectArray = ArrayHelper::
					map($allproject, 'id', 'name');
		
		$allprojectArray[null] = 'All';
		$allprojectArray = array_reverse ($allprojectArray, true );
		return $allprojectArray;
	}
}
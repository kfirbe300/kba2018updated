<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project;
use app\models\User;
use app\models\Projectpartner;

class ProjectSearch extends Project
{
    public $tag;//הוספת התכונה tags

    public function rules()
    {
        return [
            [['id', 'type', 'responsible', 'department', 'cost', 'subcontractor', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name', 'startDate', 'finishDate', 'actualfinishDate','tag'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Project::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'responsible' => $this->responsible,
            'department' => $this->department,
            'cost' => $this->cost,
            'subcontractor' => $this->subcontractor,
            'status' => $this->status,
            'startDate' => $this->startDate,
            'finishDate' => $this->finishDate,
            'actualfinishDate' => $this->actualfinishDate,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);
		
		//מניעת מבצע משימה לראות פרויקטים שלא שלו
		if(!\Yii::$app->user->can('createTask')){
			if(\Yii::$app->user->can('Perform Task')){
				$perform = Yii::$app->user->identity->id;
				
				$projectPartner = Project::find()->all();
				
				$allPartners = Projectpartner::find()->all();
				$allPartnersArray = [];
				
				foreach($projectPartner as $i){
					foreach($allPartners as $j){
						if($j->userId == $perform){
							if($i->id == $j->projectId){
								$allPartnersArray[] = $j->projectId;
							}
						}
					}
				}
				if($allPartnersArray == null){
					$query->andFilterWhere(['id' => 0]);
				}
				else
					$query->andFilterWhere(['id' => $allPartnersArray]);
			}
		}

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'subcontractor', $this->subcontractor]);
            
              //Add tags condition
            if(!empty($this->tag)){
                $condition = Tag::find()
                -> select('id')
                -> where(['IN','name',$this->tag]);
               $query->joinWith('tags');
               $query->andWhere(['IN','tag_id',$condition]);
                //here the condition will apear
                //תנאי המסנן לפי המאמרים -4 שורות קוד
            }

        return $dataProvider;
    }
}
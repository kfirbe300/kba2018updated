<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Feedback;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'dashboard'],
                'rules' => [
					[
                        'actions' => ['dashboard'],
                        'allow' => true,
                        'roles' => ['readOnly'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
				return $this->goHome();
        }
		
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			$role = Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id);
			if(implode(', ', array_keys($role)) == 'Admin')	
				return $this->redirect(['site/index']);
			else if(implode(', ', array_keys($role)) == 'Project Manager')			
				return $this->redirect(['task/index']);
			else if(implode(', ', array_keys($role)) == 'Perform Task')			
				return $this->redirect(['task/index']);
			else if(implode(', ', array_keys($role)) == 'CEO')
				return $this->redirect(['project/index']);
            else
				return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
	
	//Dashboard
	public function actionDashboard()
    {
        return $this->render('dashboard');
    }

    //feedback
    public function actionFeedback(){
        $model = new Feedback();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            Yii::$app->session->setFlash('FeedSubmitted');
            return $this->render('feedback',[
                'model' => $model,
            ]);
        }else{
            return $this->render('feedback',[
                'model' => $model,
            ]);
            
        }
    }
}
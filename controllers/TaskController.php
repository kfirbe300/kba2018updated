<?php

namespace app\controllers;

use Yii;
use app\models\Task;
use app\models\TaskSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Status;
use app\models\Project;
use app\models\Taskpartner;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;

class TaskController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['create', 'index'],
				'rules' => [
					[
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['createTask'],
					],	
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['readOnly'],
					],	
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
					'deletemultiple' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new TaskSearch();
	
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 8];
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			//'statuses' => Status::getStatusesWithAllStatuses(),
			'statuses' => Task::getExistStatusWithAllStatus(),
			'status' => $searchModel->status,
			//'projects' => Project::getProjectsWithAllStatuses(),
			'projects' => Task::getExistProjectsWithAllProjects(),
			//'projects' => $allResArray,
			'project' => $searchModel->project,
			
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Task();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		if(\Yii::$app->user->can('createTask')){
		//בדיקת הרשאה
			if (!\Yii::$app->user->can('updateTask', ['task' =>$model]) )
				throw new UnauthorizedHttpException('Hey, you are not allowed to update a task that not belong to the project you responsible for');
		}
		if(!\Yii::$app->user->can('createTask')){
			if(!\Yii::$app->user->can('Perform Task')){
				if(\Yii::$app->user->can('readOnly')){
					throw new UnauthorizedHttpException('Hey, you are not allowed to update a task');
				}
			}
		}
        
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
		$model = $this->findModel($id);
		//בדיקת הרשאה
		if (!\Yii::$app->user->can('deleteTask', ['task' =>$model]) ){
			if(\Yii::$app->user->can('createTask'))
				throw new UnauthorizedHttpException('Hey, you are not allowed to delete a task that not belong to the project you responsible for');
			if(\Yii::$app->user->can('readOnly'))
				throw new UnauthorizedHttpException('Hey, you are not allowed to perform this action.');
		}
		//מחיקת משתתפים בעת מחיקת משימה
		$selection = Taskpartner::find()->all();
		foreach($selection as $i){
			if($this->findModel($id)->id == $i->taskId){
				$i->delete();
			}
		}
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionDeletemultiple(){
       $action=Yii::$app->request->post('action');
       $selection=(array)Yii::$app->request->post('selection');
       foreach($selection as $id){
		   //בדיקה שהמשימה שייכת למשתמש האחראי על הפרויקט
		   if (\Yii::$app->user->can('updateTask', ['task' => $id]) ){
			   $e = Task::findOne(['id'=> $id]);
				//מחיקת משתתפים בעת מחיקת משימה
				$select = Taskpartner::find()->all();
				foreach($select as $i){
					if($e->id == $i->taskId)
						$i->delete();
				}
				$e->delete();
		   }
		}
		
		return $this->redirect(['index']);
	}
}
<?php

namespace app\controllers;

use Yii;
use app\models\Subcontractor;
use app\models\SubcontractorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SubcontractorController implements the CRUD actions for Subcontractor model.
 */
class SubcontractorController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['create', 'update', 'delete', 'index'],
				'rules' => [
					[
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['createUser'],
					],
					[
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['updateUser'],
					],
					[
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['deleteUser'],
					],	
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['readOnly'],
					],	
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SubcontractorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 8];
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Subcontractor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Subcontractor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionDeletemultiple(){
	   if(\Yii::$app->user->can('deleteUser')){
		   $action=Yii::$app->request->post('action');
		   $selection=(array)Yii::$app->request->post('selection');
		   foreach($selection as $id){
				$e = Subcontractor::findOne(['id'=> $id]);
				$e->delete();
		  }
	   }
		
		return $this->redirect(['index']);
	}
}
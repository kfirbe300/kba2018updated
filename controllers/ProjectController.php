<?php

namespace app\controllers;

use Yii;
use app\models\Project;
use app\models\ProjectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Status;
use app\models\Department;
use app\models\User;
use app\models\Type;
use app\models\Task;
use app\models\Projectpartner;
use app\models\Taskpartner;
use yii\web\UnauthorizedHttpException;
use yii\helpers\Html;

class ProjectController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['create', 'index', 'delete'],
				'rules' => [
					[
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['createProject'],
					],
					
					[
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['deleteProject'],
					],
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['readOnly'],
					],	
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
					'deletemultiple' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
		if(!\Yii::$app->user->can('createProject')){
			if (\Yii::$app->user->can('createTask')){
				if(!isset($params['responsible'])){
					$res = Yii::$app->user->identity->id;
					$searchModel->responsible = $res;
				}
			}
		}
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		$dataProvider->pagination = ['pageSize' => 4];
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			
			//'statuses' => Status::getStatusesWithAllStatuses(),
			'statuses' => Project::getExistStatusWithAllStatus(),
			'status' => $searchModel->status,
			
			//'departments' => Department::getDepartmentsWithAllStatuses(),
			'departments' => Project::getExistDepartmentsWithAllDepartments(),
			'department' => $searchModel->department,
			
			//'responsibles' => User::getResponsiblesWithAllStatuses(),
			'responsibles' => Project::getExistResponsiblesWithAllResponsibles(),
			'responsible' => $searchModel->responsible,
			
		
		
			//'types' => Type::getTypesWithAllStatuses(),
			'types' => Project::getExistTypesWithAllTypes(),
			'type' => $searchModel->type,
        ]);
    }

    public function actionView($id)
    {

		
		$model = $this->findModel($id);

        $tagModels = $model->tags;
        
        $tags = '';
        foreach($tagModels as $tag) {
            $tagLink = Html::a($tag->name,['project/index','ProjectSearch[tag]'=>$tag->name]);//לך לקונטרולר ארטיקל ותפעיל עליו את האקטיון אינדקס
            $tags .= ','.$tagLink;

        }
        $tags = substr($tags,1);//מסיר את הפסיק הראשון

		
		$costAll = Project::find()->where(['department' => $model->department])->sum('cost');
		$budget = $model->departmentItem->budget;
		
		if($costAll > $budget)
			Yii::$app->session->setFlash('warning', "Pay attention! the department budget is exceeded");
		
        return $this->render('view', [
			'model' => $this->findModel($id),
			'tags' => $tags,
        ]);
    }

    public function actionCreate()
    {
        $model = new Project();
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		//רק אחראי או מנהל מערכת יכול לעדכן את הפרויקט
		if (!\Yii::$app->user->can('updateProject', ['project' =>$model]) ){
			if(\Yii::$app->user->can('createTask'))
				throw new UnauthorizedHttpException('Hey, you are not allowed to update a project you not responsible for');
			if(\Yii::$app->user->can('readOnly'))
				throw new UnauthorizedHttpException('Hey, you are not allowed to perform this action.');
		}
			
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
		
    }

    public function actionDelete($id)
    {
		
		//מחיקת כל השותפים הקשורים לפרויקט
		$projectsP = Projectpartner::find()->all();
		foreach($projectsP as $i){
			if($this->findModel($id)->id == $i->projectId){
				$i->delete();
			}
		}
		//מחיקת משימות הקשורות לפרויקט
		$tasks = Task::find()->all();
		foreach($tasks as $j){
			if($this->findModel($id)->id == $j->project){
				$j->delete();
			}
		}
		
		$selec = Taskpartner::find()->all();
		foreach($tasks as $j){
			if($this->findModel($id)->id == $j->project){
				foreach($selec as $e){
					if($j->id == $e->taskId){
						$e->delete();
					}
				}
			}
		}
		
		
		
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionDeletemultiple(){
       $action=Yii::$app->request->post('action');
       $selection=(array)Yii::$app->request->post('selection');
	   if(\Yii::$app->user->can('deleteProject')){
		   foreach($selection as $id){
				$e = Project::findOne(['id'=> $id]);
				//מחיקת שותפים לפרויקט
				$select = Projectpartner::find()->all();
				foreach($select as $i){
					if($e->id == $i->projectId){
						$i->delete();
					}
				}
					
				//מחיקת משימות הקשורות לפרויקט
				$tasks = Task::find()->all();
				$selectTask = Taskpartner::find()->all();
				foreach($tasks as $j){
					if($e->id == $j->project){
						//מחיקת שותפים למשימה
						foreach($selectTask as $t){
							if($j->id == $t->taskId){
								$t->delete();
							}
						}
						$j->delete();
					}
				}
				$e->delete();
		   }
	  }
		
	return $this->redirect(['index']);
	}
}
<?php

use yii\db\Migration;

/**
 * Handles the creation of table `feedback`.
 */
class m181009_121832_create_feedback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
            'subject' => $this->string()->notNull(),
            'message' => $this->text()->notNull(),
            'email' => $this->text()->notNull(),
            'phoneNumber' => $this->integer()->notNull(),
        ]);
        }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('feedback');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_tag_assn`.
 */
class m181009_064236_create_project_tag_assn_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('project_tag_assn', [
            'project_id' => $this->integer(),
            'tag_id' => $this->integer(),
            'PRIMARY KEY(project_id, tag_id)'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('project_tag_assn');
    }
}

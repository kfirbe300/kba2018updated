<?php

use yii\db\Migration;

/**
 * Class m181014_162530_init_rbac
 */
class m181014_162530_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $Admin = $auth->createRole('Admin');
        $auth->add($Admin);

        $CEO = $auth->createRole('CEO');
        $auth->add($CEO);

        $PerformTask = $auth->createRole('Perform Task');
        $auth->add($PerformTask);

        $ProjectManager = $auth->createRole('Project Manager');
        $auth->add($ProjectManager);

        $deleteProject = $auth->createPermission('deleteProject');
        $auth->add($deleteProject);

        $deleteTask = $auth->createPermission('deleteTask');
        $auth->add($deleteTask);

        $assignRole = $auth->createPermission('assignRole');
        $auth->add($assignRole);

        $assignTask = $auth->createPermission('assignTask');
        $auth->add($assignTask);

        $createProject = $auth->createPermission('createProject');
        $auth->add($createProject);

        $createProjectpartner = $auth->createPermission('CreateProjectpartner');
        $auth->add($createProjectpartner);

        $createTask = $auth->createPermission('createTask');
        $auth->add($createTask);

        $createUser = $auth->createPermission('createUser');
        $auth->add($createUser);

        $deleteProjectpartner = $auth->createPermission('deleteProjectpartner');
        $auth->add($deleteProjectpartner);

        $deleteTaskpartner = $auth->createPermission('deleteTaskpartner');
        $auth->add($deleteTaskpartner);

        $deleteUser = $auth->createPermission('deleteUser');
        $auth->add($deleteUser);

        $onlyOwnProject = $auth->createPermission('onlyOwnProject');
        $auth->add($onlyOwnProject);

        $onlyOwnProjecttasks = $auth->createPermission('onlyOwnProjecttasks');
        $auth->add($onlyOwnProjecttasks);

        $ownProjectpartner = $auth->createPermission('ownProjectpartner');
        $auth->add($ownProjectpartner);

        $ownTaskpartner = $auth->createPermission('ownTaskpartner');
        $auth->add($ownTaskpartner);

        $projectAssociation = $auth->createPermission('projectAssociation');
        $auth->add($projectAssociation);

        $readOnly = $auth->createPermission('readOnly');
        $auth->add($readOnly);

        $updateOwnUser = $auth->createPermission('updateOwnUser');
        $auth->add($updateOwnUser);

        $updateProject = $auth->createPermission('updateProject');
        $auth->add($updateProject);

        $updateProjectpartner = $auth->createPermission('updateProjectpartner');
        $auth->add($updateProjectpartner);

        $updateTask = $auth->createPermission('updateTask');
        $auth->add($updateTask);

        $rule = new \app\rbac\OwnProjectassociationRule; 
        $auth->add($rule);

        $rule = new \app\rbac\OwnProjectRule; 
        $auth->add($rule);

        $rule = new \app\rbac\OwnProjecttasksRule; 
        $auth->add($rule);

        $rule = new \app\rbac\OwnTaskassociationRule; 
        $auth->add($rule);

        $rule = new \app\rbac\OwnUserRule; 
        $auth->add($rule);

      
        

        $onlyOwnProject->ruleName = $rule->name;                
       // $auth->add($onlyOwnProject);  
        
        $onlyOwnProjecttasks->ruleName = $rule->name;                
       // $auth->add($onlyOwnProjecttasks);  

        $ownProjectpartner->ruleName = $rule->name;                
       // $auth->add($ownProjectpartner);  

        $ownTaskpartner->ruleName = $rule->name;                
       // $auth->add($ownTaskpartner);   

        $auth->addChild($Admin, $deleteProject);
        $auth->addChild($Admin, $deleteTask);
        $auth->addChild($Admin, $assignRole);
        $auth->addChild($Admin, $updateTask);/////////////////////להוסיף!!
        $auth->addChild($onlyOwnProject, $updateTask); 
        
        $auth->addChild($Admin, $createProject);
        $auth->addChild($Admin, $createProjectpartner);
        $auth->addChild($Admin, $createUser);

        $auth->addChild($Admin, $deleteProjectpartner);
        $auth->addChild($Admin, $deleteTaskpartner);
        $auth->addChild($Admin, $deleteUser);

        $auth->addChild($Admin, $ProjectManager);
        $auth->addChild($Admin, $updateProject);
        $auth->addChild($CEO, $readOnly);

        $auth->addChild($onlyOwnProject, $deleteProject);
        $auth->addChild($onlyOwnProjecttasks, $deleteTask);

        $auth->addChild($ownProjectpartner, $createProjectpartner);
        $auth->addChild($ownProjectpartner, $deleteProjectpartner);

        $auth->addChild($ownTaskpartner, $deleteTaskpartner);

        $auth->addChild($PerformTask, $CEO);
        $auth->addChild($PerformTask, $updateOwnUser);

        $auth->addChild($ProjectManager, $createTask);
        $auth->addChild($ProjectManager, $onlyOwnProject);
        $auth->addChild($ProjectManager, $onlyOwnProjecttasks);
        $auth->addChild($ProjectManager, $ownProjectpartner);
        $auth->addChild($ProjectManager, $ownTaskpartner);

        
        $auth->addChild($ProjectManager, $PerformTask);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181014_162530_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181014_162530_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}

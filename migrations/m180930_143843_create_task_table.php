<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m180930_143843_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'project' => $this->integer()->notNull(),
			'description' => $this->text()->notNull(),
			'status' => $this->integer()->notNull(),
			'startDate' => $this->DATE()->notNull(),
			'finishDate' => $this->DATE()->notNull(),
			'actualfinishDate' => $this->DATE()->notNull(),
			'created_at' => $this->integer(),
			'created_by' => $this->integer(),
			'updated_at' => $this->integer(),
			'updated_by' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180930_143118_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
			'username' => $this->string()->notNull(),
			'firstname' => $this->string(),
			'lastname' => $this->string(),
			'password' => $this->string()->notNull(),
			'department' => $this->integer()->notNull(),
			'authKey' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}

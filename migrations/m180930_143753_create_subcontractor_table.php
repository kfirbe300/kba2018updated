<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subcontractor`.
 */
class m180930_143753_create_subcontractor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('subcontractor', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'contact' => $this->string(),
			'phone' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('subcontractor');
    }
}

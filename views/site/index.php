<?php

/* @var $this yii\web\View */


$this->title = 'K.E.R Event and Project management Company';
?>
<div class="site-index" >

    <div class="jumbotron">
        
        
        <h1 style="font-weight: bold; font-size: 500%; margin-top: 0px; margin-bottom: 0px;">Welcome to K.E.R System </h1>
        <h1 style="font-weight: bold; font-size:350%; margin-top: 0px; margin-bottom: 70px;">Projects & Events Management</h1>

<?php
	if(Yii::$app->user->isGuest){ ?>

        <div align="center" class="body-content" style="margin-top: 70px; margin-bottom: 0px;">
			<p><a class="btn btn-lg btn-primary" style="font-size:120%;" href="http://localhost/project18/web/index.php/site/login">Click to login</a></p>
				<div class="row">
<?php } ?>

    <div class="body-content">

    <div class="row">

    <?php
	if(!Yii::$app->user->isGuest){ ?>


            <div class="col-lg-3">
                <h2>Projects & Events</h2>

                <p>Create, update and delete projects.</p>

                <p><a class="btn btn-default" href="http://localhost/project18/web/index.php/project/create">Click to start &raquo;</a></p>
            </div>
            <div class="col-lg-3">
                <h2>Tasks</h2>

                <p>Create, update and delete tasks.</p>

                <p><a class="btn btn-default" href="http://localhost/project18/web/index.php/task/create">Click to start &raquo;</a></p>
            </div>
            <div class="col-lg-3">
                <h2>Project Partners</h2>

                <p>Associate users to a project.</p>

                <p><a class="btn btn-default" href="http://localhost/project18/web/index.php/projectpartner/create">Click to start &raquo;</a></p>
            </div>
            <div class="col-lg-3">
                

            </div>
			<div class="col-lg-3">
                <h2>Task Partners</h2>

                <p>Associate users to a task.</p>

                <p><a class="btn btn-default" href="http://localhost/project18/web/index.php/taskpartner/create">Click to start &raquo;</a></p>
            </div>
            <div class="col-lg-3">
            </div>
        </div>

        <?php } ?>

        
        <?php
	if(Yii::$app->user->isGuest){ ?>

            <div align="center" class="body-content" style="margin-top: 70px; margin-bottom: 135px;">
            <h2>NOTE: According to our records, you are not allowed to view the data. Click the 'login' button to login or contact your manager for more information.</h2>
            <?php } ?>
    </div>
</div>

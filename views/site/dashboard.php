<?php
use yii\helpers\Html;
use app\models\User;
use app\models\Project;
use app\models\Projectpartner;
use app\models\Department;
use app\models\Task;
use app\models\Taskpartner;
use yii\bootstrap\Progress;
use yii\bootstrap\BootstrapWidgetTrait;
use yii\grid\GridView;
use macgyer\yii2materializecss\lib\MaterializeWidgetTrait;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;

//כללי
$budget = Department::find()->where('id')->sum('budget'); 
$projects = Project::find()->where('id')->sum('cost') - Project::find()->where(['status'=>2])->sum('cost');
$numProjects = Project::find()->where('id')->count('id') - Project::find()->where(['status'=>2])->count('id') ;
$numTasks = Task::find()->where('id')->count('id') - Task::find()->where(['status'=>2])->count('id') ;
$users = User::find()->where('id')->count('id');
if($budget != 0)
				$precent = round(($projects/$budget)*100);
			else
				$precent = 0;
//פרויקטים
$numDone = Project::find()->where(['status' => 5])->count('id');
$numPro = Project::find()->where(['status' => 3])->count('id');
$numPen = Project::find()->where(['status' => 4])->count('id');
//$numClose = Project::find()->where(['status' => 2])->count('id');
if($numProjects != 0)
				$totalPro = round(($numDone/($numProjects))*100);
			else
				$totalPro = 0;

//משימות
$numDoneT = Task::find()->where(['status' => 5])->count('id');
$numProT = Task::find()->where(['status' => 3])->count('id');
$numPenT = Task::find()->where(['status' => 4])->count('id');
$totalT = round(($numDoneT/($numTasks))*100);
if($numTasks != 0)
				$totalT = round(($numDoneT/($numTasks))*100);
			else
                $totalT = 0;

?>


<div style="color: #000000;">
	<div style="margin:0 0 0 auto;">
        <h1 style="font-weight: bold; font-size: 200%;">Dashboard</h1>
	</div>

    </div>
<p class="lead" style="font-size: 150%; margin:0 0 0 auto;">Percentage of budget utilization 💰</p>
<?= Progress::widget([
    'percent' => $precent,
	'label' => $precent.'% Used',
    'barOptions' => ['class' => 'progress-bar-success'],
    'options' => ['class' => 'active progress-striped']
]) ?>
<p class="lead" style="font-size: 150%; margin:0 0 0 auto;">Percentage of progress in projects ⚠️</p>
<?= Progress::widget([
    'percent' => $totalPro,
	'label' => $totalPro.'% Completed',
    'barOptions' => ['class' => 'progress-bar-warnning',],
	'options' => ['class' => 'active progress-striped']
]) ?>

<p class="lead" style="font-size: 150%; margin:0 0 0 auto;">Percentage of progress in tasks ⏳</p>
<?= Progress::widget([
    'percent' => $totalT,
	'label' => $totalT.'% Completed',
    'barOptions' => ['class' => 'progress-bar-warnning',],
	'options' => ['class' => 'active progress-striped']
]) ?>
<div style="">
	
	<p class="lead" style="font-size: 180%; margin:0 0 0 auto;">General</p>
	<div style="margin:0 0 0 auto;">
			<!--<div class="col-lg-3">-->
			<?php 
			$budget = Department::find()->where('id')->sum('budget'); 
			$projects = Project::find()->where('id')->sum('cost');
			$numProjects = Project::find()->where('id')->count('id');
			$numTasks = Task::find()->where('id')->count('id');
			$users = User::find()->where('id')->count('id');
			$precent = round(($projects/$budget)*100);
			
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style:double; background-color: #A9F5F2; '><b>Budget ($):</b> $budget</p>";
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style: double;'><b>Budget use ($):</b> $projects</p>";
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style: double;' step='0.01'><b>Budget utilization:</b> $precent%</p>";
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style: double;'><b>Number of users:</b> $users</p>";
			
		
			?>
			<div class="progress">
			  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
				<?= Progress::widget([
					 'percent' => 70,
					'barOptions' => ['class' => 'progress-bar-success'],
					'options' => ['class' => 'active progress-striped'],
				]) ?>
				</div>
			</div>
			<?php /*GridView::widget([
			'columns' => [
				Progress::widget(
				[
					'class' => 'booster.widgets.TbProgress',
					'percent' => 70,
					'barOptions' => ['class' => 'progress-bar-success'],
					'options' => ['class' => 'active progress-striped'],
				])
				],
			]) */?>
			
	</div>
	<p class="lead" style="font-size: 180%; margin:0 0 0 auto;">Projects</p>
	<div style="margin:0 0 0 auto;">
			<!--<div class="col-lg-3">-->
			<?php 
			$numDone = Project::find()->where(['status' => 5])->count('id');
			$numPro = Project::find()->where(['status' => 3])->count('id');
			$numPen = Project::find()->where(['status' => 4])->count('id');
			
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style: double;background-color: #A9F5F2; '><b>Number of projects:</b> $numProjects</p>";
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style: double;'><b>Number in status-Done:</b> $numDone</p>";
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style: double;'><b>Number in status-In Process:</b> $numPro</p>";
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style: double;'><b>Number in status-Pending:</b> $numPen</p>";
			?>	
	</div>
	
	<p class="lead" style="font-size: 180%; margin:60px 0 0 auto;">Tasks</p>
	<div style="margin:0 0 0 auto;">
			<!--<div class="col-lg-3">-->
			<?php 
			$numDoneT = Task::find()->where(['status' => 1])->count('id');
			$numProT = Task::find()->where(['status' => 4])->count('id');
			$numPenT = Task::find()->where(['status' => 3])->count('id');
			
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style: double;background-color: #A9F5F2;'><b>Number of tasks:</b> $numTasks</p>";
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style: double;'><b>Number in status-Done:</b> $numDoneT</p>";
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style: double;'><b>Number in status-In Process:</b> $numProT</p>";
			echo "<p class='col-lg-3' style='font-size: 130%; margin:0 0 0 auto; border-style: double;'><b>Number in status-Pending:</b> $numPenT</p>";
			?>	
	</div>
</div>
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About K.E.R Projects & Events company';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    
   <p> Our company specializes in managing events and projects.</p>
   <p> Our representative will accompany you from the planning stage to the implementation stage.</p>
   <p> for more information call: +972505555555</p>
   <p> Or leave an email: k.e.r@org.com and our representative will contact you shortly</p>
    
    

</div>

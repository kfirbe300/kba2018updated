<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Send your Feedback About K.E.R';

?>

<div class="ct1">
 
 <?php if(Yii::$app->session->hasFlash('FeedSubmitted')): ?>
 <div class="row">
 <div class="col-lg-4">&nbsp;;</div>
 <div class="col-lg-5">
  <div class="panel panel-default">
  <div class="panel-heading">Massage Sent</div>
  <div class="panel-body">
  <p><b>Subject:</b><?=$model->subject?></p>
  <p><b>Your Message:</b><?=$model->message?></p>
  </div>
</div>
<div class="alert alert-success">
Thank you for your Feedback. We will respond as soon as possible.
</div>
</div>
</div>
<?php else:?>

<div class="row ct">
  <div class="col-lg-3"></div>
  <div class="col-lg-6 panel panel-default bd">
  <div><h1 align="center"><?= Html::encode($this->title)?></h1></div>
  <?php $form = ActiveForm::begin(['id'=>'grievance-form']);?>
  <?=$form->field($model,'subject')?>
  <?=$form->field($model,'message')->textArea(['rows' =>6])?>
  <?=$form->field($model,'email')?>
  <?=$form->field($model,'phoneNumber')?>

  <div class= "form-group">
  <?=Html::submitButton('<i class="glyphicon glyphicon-send"></i>  Submit',['class'=>'btn btn-primary','name'=>'send-button'])?>
  </div>
  <?php ActiveForm::end(); ?>
  </div>
  </div>
<?php endif; ?>

</div>

 
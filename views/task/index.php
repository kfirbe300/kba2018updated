<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Project;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
    <p>
        <?= Html::a('Create Task', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } 
	$this->registerJs("$('.checked').click(function(){
			var keys = $('#w0').yiiGridView('getSelectedRows');
		   if($(this).is(':checked')) 
			   $(this).parent().parent().children().addClass('color');
		   else
			   $(this).parent().parent().children().removeClass('color');

		});");
	$this->registerJs("$('.select-on-check-all').change(function(){
			if($('.checked').is(':checked')) 
			   $('.checked').parent().parent().children().addClass('color');
			else
			   $('.checked').parent().parent().children().removeClass('color');

		});");
	?>
	<?=Html::beginForm(['controller/bulk'],'post');?>

	<?=Html::a('','action')?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'rowOptions'=> function($model){
            if($model->finishDate < date('Y-m-d')){
                return ['class' => 'danger'];
			}
         },
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
			'checkboxOptions' => function($model, $key, $index, $widget) {
				return ['value' => $model['id'], 'class' => 'checked']; },],

            //'id',
            'name',
            //'project', 
			[
				'attribute' => 'project',
				'label' => 'Project',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->projectItem->name, 
					['project/view', 'id' => $model->projectItem->id]);
				},
				//'filter'=>Html::dropDownList('TaskSearch[project]', $project, $projects, ['class'=>'form-control'], array('prompt' => 'Select', 'multiple' => true, 'selected' => 'selected')),
				'filter' => Html::activeDropDownList($searchModel, 'project', $projects, 
					[
						['class'=>'select2', 'multiple'=>'multiple']
					]),
			],
            //'description:ntext',
            //'status',
			[
				'attribute' => 'status',
				'label' => 'Status',
				'value' => function($model){
					return $model->statusItem->name;
				},
				'filter'=>Html::dropDownList('TaskSearch[status]', $status, $statuses, ['class'=>'form-control']),
			],
			
			/*[
				'attribute' => 'responsible',
				'label' => 'Responsible',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->projectItem->responsibleItem->fullname, 
					['user/view', 'id' => $model->projectItem->responsibleItem->id]);
				},
				'filter'=>Html::dropDownList('TaskSearch[responsible]', $responsible, $responsibles, ['class'=>'form-control']),
			],*/
            //'startDate',
            //'finishDate',
			[
				'attribute' => 'startDate',
				'label' => 'Start Date',
				'value' => 'startDate',
				'format' => 'raw',
				'filter' => DatePicker::widget([
								'model' => $searchModel,
								'attribute' => 'startDate', 
								'value' => date('Y-M-D', strtotime('+2 days')),
								'options' => ['placeholder' => 'Select issue date ...'],
								'pluginOptions' => [
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true
								]
							]),
			],
			[
				'attribute' => 'finishDate',
				'label' => 'Finish Date',
				'value' => 'finishDate',
				'format' => 'raw',
				'filter' => DatePicker::widget([
								'model' => $searchModel,
								'attribute' => 'finishDate', 
								'value' => date('Y-M-D', strtotime('+2 days')),
								'options' => ['placeholder' => 'Select issue date ...'],
								'pluginOptions' => [
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true
								]
							]),
			],
            // 'actualfinishDate',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
	
	<?php } ?>
</div>
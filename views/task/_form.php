<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Project;
use app\models\Status;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	
	<?php if(\Yii::$app->user->can('createProject')){ ?>
		<?= $form->field($model, 'project')->dropDownList(Project::getProjects(),['prompt'=>'Choose a project',])  ?>
	<?php } 
	
	else{ ?>
		<?= $form->field($model, 'project')->dropDownList(Project::getProjectofuser(),['prompt'=>'Choose a project',])  ?>
	<?php } ?>
	
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
	
	<?php } ?>

    <?= $form->field($model, 'status')->dropDownList(Status::getStatuses(),['prompt'=>'Choose a status of task',])?>
	
	<?php if(\Yii::$app->user->can('createTask')){ ?>
	 <?= $form->field($model, 'startDate')->widget(DatePicker::className(),[
								'name' => 'startDate', 
								'value' => date('Y-M-D'),
								'options' => [
									//'value' => date('Y-m-d'),
								],
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
									'clearField' => true,
									'todayHighlight' => true,
								]
							]) ?>
	
	<?= $form->field($model, 'finishDate')->widget(DatePicker::className(),[
								'name' => 'finishDate', 
								'value' => date('Y-M-D'),
								'options' => [
									//'value' => date('Y-m-d'),
								],
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true,
									'clearField' => true,
								],
							]) ?>
	<?php } ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Task;
use app\models\Projectpartner;
use yii\helpers\ArrayHelper;

?>

<div class="task-partner-form">

    <?php $form = ActiveForm::begin(); ?>

	<?php /* $form->field($model, 'taskId')->dropDownList(ArrayHelper::map(Task::find()->all(), 'id', 'name'), 
             ['prompt'=>'-Choose a Task-',
              'onchange'=>'
                $.get( "lists&taskId="+$(this).val()+"&userId="+$(this).val(), function( data ) {
                  $( "#taskpartner-taskId" ).val( data );
                });
            ']);*/
    ?>
	
	<?php if(\Yii::$app->user->can('createProject')){ ?>
		<?= $form->field($model, 'taskId')->dropDownList(Task::getTasks(), 
                                     ['prompt'=>'Choose a task',
										'onchange'=>' $.post( "list?id='.'"+$(this).val(), function( data ) {
											$( "select#taskpartner-userid" ).html( data );
									
									}); ']); ?>
	<?php } 
	
	else{ ?>
	
	<?= $form->field($model, 'taskId')->dropDownList(Task::getTasksofuser(), 
                                     ['prompt'=>'Choose a Task',
										'onchange'=>' $.post( "list?id='.'"+$(this).val(), function( data ) {
												$( "select#taskpartner-userid" ).html( data );
									
												}); ']); ?>
	<?php } ?>

     <?= $form->field($model, 'userId')
                    ->dropDownList(
                        User::getTaskspartners(),
						['prompt'=>'Choose a partner',]
						
                    ); ?>
	
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
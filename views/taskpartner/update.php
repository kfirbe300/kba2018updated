<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskPartner */

$this->title = 'Update Task Partner: '; //. $model->userId;
$this->params['breadcrumbs'][] = ['label' => 'Task Partners', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->userId, 'url' => ['view', 'userId' => $model->userId, 'taskId' => $model->taskId]];
$this->params['breadcrumbs'][] = ['label' => $model->fullname, 'url' => ['view', 'userId' => $model->userId, 'taskId' => $model->taskId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="task-partner-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
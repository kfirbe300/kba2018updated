<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskPartnerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Task Partners';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="task-partner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
    <p>
        <?= Html::a('Add Task Partner', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } 
	$this->registerJs("$('.checked').click(function(){
			var keys = $('#w0').yiiGridView('getSelectedRows');
		   if($(this).is(':checked')) 
			   $(this).parent().parent().children().addClass('color');
		   else
			   $(this).parent().parent().children().removeClass('color');

		});");
	$this->registerJs("$('.select-on-check-all').change(function(){
			if($('.checked').is(':checked')) 
			   $('.checked').parent().parent().children().addClass('color');
			else
			   $('.checked').parent().parent().children().removeClass('color');

		});");
	?>
	<?=Html::beginForm(['controller/bulk'],'post');?>

	<?=Html::a('','action')?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
			'checkboxOptions' => function($model, $key, $index, $widget) {
				return ['value' => $model['userId'], 'class' => 'checked']; },],

            //'userId',
			[
				'attribute' => 'userId',
				'label' => 'User',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->userItem->fullname, 
					['user/view', 'id' => $model->userItem->id]);
				},
				'filter'=>Html::dropDownList('TaskpartnerSearch[userId]', $user, $users, ['class'=>'form-control']),
			],
            //'taskId',
			[
				'attribute' => 'taskId',
				'label' => 'Task',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->taskItem->name, 
					['task/view', 'id' => $model->taskItem->id]);
				},
				'filter'=>Html::dropDownList('TaskpartnerSearch[taskId]', $task, $tasks, ['class'=>'form-control']),
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
	
	<?php } ?>
</div>
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Home page',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
            'style' => 'color:#340068',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
           // ['label' => 'Home page', 'url' => ['/site/index']],
         //   ['label' => 'About', 'url' => ['/site/about']],
          //  ['label' => 'Contact', 'url' => ['/site/contact']],
          ['label' => 'FeedBack', 'url' => ['/site/feedback']],
          ['label' => 'About K.E.R', 'url' => ['/site/about']],
          ['label' => 'Dashboard', 'url' => ['/site/dashboard']],
            ['label' => 'Users', 'url' => ['/user/index']],
			['label' => 'Projects', 'url' => ['/project/index'],'items' => [
				['label' => 'View Projects', 'url' => ['/project/index']],
				['label' => 'View Partners', 'url' => ['/projectpartner/index']],
			]],
			['label' => 'Tasks', 'url' => ['/task/index'] ,'items' => [
				['label' => 'View Tasks', 'url' => ['/task/index']],
				['label' => 'View Partners', 'url' => ['/taskpartner/index']],
			]],
			['label' => 'More','items' => [
				['label' => 'Departments', 'url' => ['/department/index']],
				['label' => 'Subcontractors', 'url' => ['/subcontractor/index']],
			]],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; K.E.R Event and Project management Company <?= date('Y') ?></p>

        <p class="pull-right"><?php //  Yii::powered() ?>💻 Analysis and Planning Information Systems 2 💻</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

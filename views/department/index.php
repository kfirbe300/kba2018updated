<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departments';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.color{
	background-color: #ccebff;
}
.grid-view table tbody tr:hover
{
	background-color: #B0C4DE;
}
</style>
<div class="department-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if(\Yii::$app->user->can('createUser')){ ?>
    <p>
        <?= Html::a('Create Department', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php }  
	
	$this->registerJs("$('.checked').click(function(){
			var keys = $('#w0').yiiGridView('getSelectedRows');
		   if($(this).is(':checked')) 
			   $(this).parent().parent().children().addClass('color');
		   else
			   $(this).parent().parent().children().removeClass('color');

		});");
	$this->registerJs("$('.select-on-check-all').change(function(){
			if($('.checked').is(':checked')) 
			   $('.checked').parent().parent().children().addClass('color');
			else
			   $('.checked').parent().parent().children().removeClass('color');

		});");
	?>
	<?=Html::beginForm(['controller/bulk'],'post');?>

	<?=Html::a('','action')?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
			'checkboxOptions' => function($model, $key, $index, $widget) {
				return ['value' => $model['id'], 'class' => 'checked']; },],

            //'id',
            'name',
            //'headDepartment',
			[
				'attribute' => 'headDepartment',
				'label' => 'Head Department',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->userItem->fullname, 
					['user/view', 'id' => $model->userItem->id]);
				},
			
			],
            'budget',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php if(\Yii::$app->user->can('deleteUser')){ ?>

	<?php } ?>
</div>
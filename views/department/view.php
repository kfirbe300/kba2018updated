<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Department */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	<?php if(\Yii::$app->user->can('updateUser')){ ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
	<?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            //'headDepartment',
			[ 				
				'label' => $model->attributeLabels()['headDepartment'],
				'format' => 'html', 
				'value' => Html::a($model->userItem->fullname, 
					['user/view', 'id' => $model->userItem->id]),	
			],
            'budget',
        ],
    ]) ?>
	
	<?php
	//הצגת של המשתמשים הקשורים למחלקה
	if($model->usersItem != null){
		echo '<div class="col-lg-4">';
		echo '<label style="font-size: 150%;"> Users: </label><br>';
		echo "|";
		foreach($model->usersItem as $i){
			echo ' <a href="'.URL::to(['user/view', 'id' => $i->id]).'" style="font-size: 150%;">'.$i->fullname.'</a> |'; 
		}
		
		echo "</div>";
		
	}
	
	
	if($model->projectsItem != null){
		echo '<div class="col-lg-4">';
		echo '<label style="font-size: 150%;"> Projects: </label><br>';
		echo "|";
		foreach($model->projectsItem as $i){
			echo ' <a href="'.URL::to(['project/view', 'id' => $i->id]).'" style="font-size: 150%;">'.$i->name.'</a> |'; 
		}
		
		echo "</div>";
		echo "<br><br><br><br><br>";
	}
	?>

</div>
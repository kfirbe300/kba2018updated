<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if(\Yii::$app->user->can('createUser')){ ?>
    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } 
	
	$this->registerJs("$('.checked').click(function(){
			var keys = $('#w0').yiiGridView('getSelectedRows');
		   if($(this).is(':checked')) 
			   $(this).parent().parent().children().addClass('color');
		   else
			   $(this).parent().parent().children().removeClass('color');

		});");
	$this->registerJs("$('.select-on-check-all').change(function(){
			if($('.checked').is(':checked')) 
			   $('.checked').parent().parent().children().addClass('color');
			else
			   $('.checked').parent().parent().children().removeClass('color');

		});");
	?>
	<?=Html::beginForm(['controller/bulk'],'post');?>

	<?=Html::a('','action')?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
			'checkboxOptions' => function($model, $key, $index, $widget) {
				return ['value' => $model['id'], 'class' => 'checked','id' => 'checked']; }, 
			],

            //'id',
            'username',
            'firstname',
            'lastname',
            //'password',
            //'department',
			[
				'attribute' => 'department',
				'label' => 'Department',
				/*'value' => function($model){
					return $model->departmentItem->name;
				},*/
				'format' => 'html',
				'value' => function($model){
					if($model->departmentItem){
						return Html::a($model->departmentItem->name, 
							['department/view', 'id' => $model->departmentItem->id]);
					}
					else {
						return 'General';
					}
				},
				'filter'=>Html::dropDownList('UserSearch[department]', $department, $departments, ['class'=>'form-control']),
			],
            // 'authKey',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php if(\Yii::$app->user->can('deleteUser')){ ?>
	
	<?php } ?>
</div>
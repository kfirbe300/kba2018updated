<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Department;
use app\models\Type;
use app\models\User;
use app\models\Status;
use app\models\Subcontractor;
use kartik\date\DatePicker;
use dosamigos\selectize\SelectizeTextInput;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	<?php if(\Yii::$app->user->can('createProject')){ ?>
		<?= $form->field($model, 'type')->dropDownList(Type::getTypes(),['prompt'=>'Choose a type of project',]) ?>
	<?php } ?>
	<?php if(\Yii::$app->user->can('createProject')){ ?>
		<?= $form->field($model, 'responsible')->dropDownList(User::getManagers(),['prompt'=>'Choose a responsible',]) ?>
	<?php } ?>
	
    <?php //$form->field($model, 'department')->dropDownList(Department::getDepartments()) ?>

    <?= $form->field($model, 'cost')->textInput() ?>

    <?= $form->field($model, 'subcontractor')->dropDownList(Subcontractor::getSubcontractors(),['prompt'=>'Choose a subcontractor',]) ?>

    <?= $form->field($model, 'status')->dropDownList(Status::getStatuses(),['prompt'=>'Choose a status of project',]) ?>
	
	<?= $form->field($model, 'tagNames')->widget(SelectizeTextInput::className(), [
    // calls an action that returns a JSON object with matched
    // tags
    'loadUrl' => ['tag/list'],
    'options' => ['class' => 'form-control'],
    'clientOptions' => [
        'plugins' => ['remove_button'],
        'valueField' => 'name',
        'labelField' => 'name',
        'searchField' => ['name'],
        'create' => true,
    ],
])->hint('Use commas to separate tags') ?>

	<?php if(\Yii::$app->user->can('createUser')){ ?>
    <?= $form->field($model, 'startDate')->widget(DatePicker::className(),[
								'name' => 'startDate', 
								'value' => date('Y-M-D'),
								/*'options' => [
									'value' => date('Y-m-d'),
								],*/
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
									'clearField' => true,
									'todayHighlight' => true,
								]
							]) ?>

	
							
	<?= $form->field($model, 'finishDate')->widget(DatePicker::className(),[
								'name' => 'finishDate', 
								'value' => date('Y-M-D'),
								'options' => [
									//'value' => date('Y-m-d'),
									
								],
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true,
									'clearField' => true,
								],
							]) ?>
	<?php } ?>

   


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
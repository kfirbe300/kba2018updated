<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Project;
/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	<?php if(\Yii::$app->user->can('updateUser')){ ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
	<?php } ?>
    </p>
	
	<?php if(Yii::$app->session->hasFlash('warning')): ?>
	  <div class="alert alert-success alert-dismissable" style="background-color: red; color: white;">
	  <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="color: white;">×</button>
		<?= Yii::$app->session->getFlash('warning') ?>
	  </div>
	<?php endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            //'type',
			[ 
				'label' => $model->attributeLabels()['type'],
				'value' => $model->typeItem->type,	
			],
            //'responsible',
			[ 
				'label' => $model->attributeLabels()['responsible'],
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->responsibleItem->fullname, 
					['user/view', 'id' => $model->responsibleItem->id]);
				},	
			],
            //'department',
			[ 
				'label' => $model->attributeLabels()['department'],
				//'value' => $model->departmentItem->name,	
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->departmentItem->name, 
					['department/view', 'id' => $model->departmentItem->id]);
				},
			],
            'cost',
            //'subcontractor',
			[ 
				'label' => $model->attributeLabels()['subcontractor'],
				'value' => $model->subcontractorItem->name,	
				'value' => function($model){
					if($model->subcontractorItem){
						return $model->subcontractorItem->name;
					}
					else {
						return 'No subcontractor';
					}
				},
			],
            //'status',
			[ 
				'label' => $model->attributeLabels()['status'],
				'value' => $model->statusItem->name,	
			],
			//'tag'
			[
                'label' => 'Tags',
                'format' => 'html',
                'value' => $tags
            ],

            'startDate',
            'finishDate',
            //'actualfinishDate',
			[
				'label' => $model->attributeLabels()['actualfinishDate'],
				'value' => function($model){
					if(($model->status == 1)){
						return $model->actualfinishDate;
					}
					else {
						return 'Not yet finished';
					}
				},
			],
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',
			/*[ 
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],*/
			[ 
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->fullname) ? $model->createdBy->fullname : 'No one!',	
			],
            [ 
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],	
            [ 
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->fullname) ? $model->updateddBy->fullname : 'No one!',	
			],
        ],
    ]) 
	?>
	
	<?php

	//הצגת כל המשימות הקשורות לפרויקט
	if($model->tasksItem != null){
		echo '<div class="col-lg-3">';
		echo '<label style="font-size: 150%;"> Tasks: </label><br>';
		echo "|";
		foreach($model->tasksItem as $i){
			echo ' <a href="?r=task/view&id='.$i->id.'" style="font-size: 150%;">'.$i->name.'</a> |';  
		}
		//echo "<br><br>";
		echo "</div>";
		//echo "<br><br>";
	}
	else{
		echo "<br><br>";
	}
	?>
	
	<?php
	//הצגת כל המשתמשים הקשורים לפרויקט
	if($model->usersItem != null){
		echo '<div class="col-lg-3">';
		echo '<label style="font-size: 150%;"> Partners: </label><br>';
		echo "|";
		foreach($model->usersItem as $i){
			echo ' <a href="?r=user/view&id='.$i->userId.'" style="font-size: 150%;">'.$i->userItem->fullname.'</a> |'; 
		}
		//echo "<br><br><br><br>";
		echo "</div>";
		echo "<br><br><br><br><br>";
	}
	else{
		echo "<br><br>";
	}
	?>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
	<div class="form-group">
		<p><br><br></p>
       <p><a class="btn btn-default" href="http://kfirbe.myweb.jce.ac.il/project18/basic/web/index.php?r=projectpartner/create">Add users &raquo;</a>
	   <a class="btn btn-default" href="http://kfirbe.myweb.jce.ac.il/project18/basic/web/index.php?r=task/create">Add Tasks &raquo;</a></p>
    </div>

	<?php } ?>

</div>
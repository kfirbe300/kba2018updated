<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use app\models\Project;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if(\Yii::$app->user->can('createProject')){ ?>
    <p>
        <?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } 
	$this->registerJs("$('.checked').click(function(){
		var keys = $('#w0').yiiGridView('getSelectedRows');
	   if($(this).is(':checked')) 
		   $(this).parent().parent().children().addClass('color');
	   else
		   $(this).parent().parent().children().removeClass('color');

	});");
$this->registerJs("$('.select-on-check-all').change(function(){
		if($('.checked').is(':checked')) 
		   $('.checked').parent().parent().children().addClass('color');
		else
		   $('.checked').parent().parent().children().removeClass('color');

	});");?>
	<?=Html::beginForm(['controller/bulk'],'post');?>

	<?=Html::a('','action')?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'rowOptions'=> function($model){
            if($model->finishDate < date('Y-m-d')){
                return ['class' => 'danger'];
			}
         },
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
			'checkboxOptions' => function($model, $key, $index, $widget) {
				return ['value' => $model['id']]; },],

            //'id',
            'name',
            //'type',
			[
				'attribute' => 'type',
				'label' => 'Type',
				'value' => function($model){
					return $model->typeItem->type;
				},
				'filter'=>Html::dropDownList('ProjectSearch[type]', $type, $types, ['class'=>'form-control']),
			],
			[
				'attribute' => 'responsible',
				'label' => 'Responsible',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->responsibleItem->fullname, 
					['user/view', 'id' => $model->responsibleItem->id]);
				},
				'filter'=>Html::dropDownList('ProjectSearch[responsible]', $responsible, $responsibles, ['class'=>'form-control']),
			],
			[
				'attribute' => 'department',
				'label' => 'Department',
				/*'value' => function($model){
					return $model->departmentItem->name;
				},*/
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->departmentItem->name, 
					['department/view', 'id' => $model->departmentItem->id]);
				},
				'filter'=>Html::dropDownList('ProjectSearch[department]', $department, $departments, ['class'=>'form-control']),
			],
            // 'cost',
            // 'subcontractor',
			[
				'attribute' => 'status',
				'label' => 'Status',
				'value' => function($model){
					return $model->statusItem->name;
				},
				'filter'=>Html::dropDownList('ProjectSearch[status]', $status, $statuses, ['class'=>'form-control']),
			],
            //'startDate',
			[
				'attribute' => 'startDate',
				'label' => 'Start Date',
				'value' => 'startDate',
				'format' => 'raw',
				'filter' => DatePicker::widget([
								'model' => $searchModel,
								'attribute' => 'startDate', 
								'value' => date('Y-M-D', strtotime('+2 days')),
								'options' => ['placeholder' => 'Select issue date ...'],
								'pluginOptions' => [
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true
								]
							]),
			],
            //'finishDate',
			[
				'attribute' => 'finishDate',
				'label' => 'Finish Date',
				'value' => 'finishDate',
				'format' => 'raw',
				'filter' => DatePicker::widget([
								'model' => $searchModel,
								'attribute' => 'finishDate', 
								'value' => date('Y-M-D', strtotime('+2 days')),
								'options' => ['placeholder' => 'Select issue date'],
								'pluginOptions' => [
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true
								]
							]),
			],
            // 'actualfinishDate',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php if(\Yii::$app->user->can('createProject')){ ?>
	
	<?php } ?>
</div>
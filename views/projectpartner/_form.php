<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Project;
/* @var $this yii\web\View */
/* @var $model app\models\ProjectPartner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-partner-form">

    <?php $form = ActiveForm::begin(); ?>
	<?php
		//$idProject[0] = Project::getProjectss();
	?>

	<?php if(\Yii::$app->user->can('createProject')){ ?>
		<?php //$form->field($model, 'projectId')->dropDownList(Project::getProjects(),['prompt'=>'Choose a project',]) ?>
		<?= $form->field($model, 'projectId')->dropDownList(Project::getProjects(), 
                                     ['prompt'=>'Choose a project',
										'onchange'=>' $.post( "list?id='.'"+$(this).val(), function( data ) {
											$( "select#projectpartner-userid" ).html( data );
									
									}); ']); ?>
	<?php } 
	
	else{ ?>
		<?php //$form->field($model, 'projectId')->dropDownList(Project::getProjectofuser(),['prompt'=>'Choose a project',]) ?>
		<?= $form->field($model, 'projectId')->dropDownList(Project::getProjectofuser(), 
                                     ['prompt'=>'Choose a project',
										'onchange'=>' $.post( "list?id='.'"+$(this).val(), function( data ) {
											$( "select#projectpartner-userid" ).html( data );
									
									}); ']); ?>
	<?php } ?>
	
	<?= $form->field($model, 'userId')
                    ->dropDownList(
                        User::getProjectpartners(),
						['prompt'=>'Choose a partner',]
						
                    ); ?>
					
	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
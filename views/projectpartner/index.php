<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectPartnerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Project Partners';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="project-partner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
    <p>
        <?= Html::a('Add Project Partner', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } 
	$this->registerJs("$('.checked').click(function(){
			var keys = $('#w0').yiiGridView('getSelectedRows');
		   if($(this).is(':checked')) 
			   $(this).parent().parent().children().addClass('color');
		   else
			   $(this).parent().parent().children().removeClass('color');

		});");
	$this->registerJs("$('.select-on-check-all').change(function(){
			if($('.checked').is(':checked')) 
			   $('.checked').parent().parent().children().addClass('color');
			else
			   $('.checked').parent().parent().children().removeClass('color');

		});");
	?>
	<?=Html::beginForm(['controller/bulk'],'post');?>

	<?=Html::a('','action')?>

	<?php //Html::submitButton('Send', ['class' => 'btn btn-info',]);?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
			'checkboxOptions' => function($model, $key, $index, $widget) {
				return ['value' => $model['userId'], 'class' => 'checked']; },], //'yii\grid\SerialColumn'
            //'userId',
			[
				'attribute' => 'userId',
				'label' => 'User',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->userItem->fullname, 
					['user/view', 'id' => $model->userItem->id]);
				},
				'filter'=>Html::dropDownList('ProjectpartnerSearch[userId]', $user, $users, ['class'=>'form-control']),
			],
            //'projectId',
			[
				'attribute' => 'projectId',
				'label' => 'Project',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->projectItem->name, 
					['project/view', 'id' => $model->projectItem->id]);
				},
				'filter'=>Html::dropDownList('ProjectpartnerSearch[projectId]', $project, $projects, ['class'=>'form-control']),
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
	
	<?php } ?>
</div>
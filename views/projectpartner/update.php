<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectPartner */

$this->title = 'Update Project Partner: '; //. $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Project Partners', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->userId, 'url' => ['view', 'userId' => $model->userId, 'projectId' => $model->projectId]];
$this->params['breadcrumbs'][] = ['label' => $model->fullname, 'url' => ['view', 'userId' => $model->userId, 'projectId' => $model->projectId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="project-partner-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>